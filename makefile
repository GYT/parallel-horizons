MARKDOWN=$(shell find . -iname "*.pd")
EPUBS=$(MARKDOWN:.pd=.epub)

%.epub: %.pd
	pandoc -f markdown+smart -t epub --css=epub.css $< -o $@

.PHONY = all clean

all: $(EPUBS)

clean:
	rm stories/*/*.epub
