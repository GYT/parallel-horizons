# Parallel Horizons

Parallel horizons is a writing project that consists of writing one short story
per week for one year, from September 1, 2018 to August 31, 2019.
This repository offers the translation of some of SAID's texts.

The fully translated works are available on
[Smashwords](https://www.smashwords.com/books/byseries/36629).

# Support us

## SAID, the author

- [Website](https://saidwords.org)
- [Tipee](http://www.tipeee.com/s-a-i-d)

## Guy Therrien, the translator

- Litecoin: LZSv1eXHKqo589f83Ss2oZU6KK23vKyZNe